// Routes
var ApiController = require('./api.controller');
var api           = new ApiController();

module.exports = [
	{
		// Get prices by instance type
		method: 'GET',
		path: '/api/prices/instance/{instanceType}',
		handler: function(req, reply) {
			api.getPricesByInstance(req.params.instanceType).then(function(data) {
				reply(data);
			});
		}
	},
	{
		// Get prices by region
		method: 'GET',
		path: '/api/prices/region/{regionType}',
		handler: function(req, reply) {
			api.getPricesByRegion(req.params.regionType).then(function(data) {
				reply(data);
			});
		}
	},
	{
		// Get prices sorted by price per vCPU
		method: 'GET',
		path: '/api/prices/vcpu',
		handler: function(req, reply) {
			api.getTopInstancesByVCPU().then(function(data) {
				reply(data);
			})
		}
	},
	{
		// Get the cheapest regions
		method: 'GET',
		path: '/api/prices/cheapest-region',
		handler: function(req, reply) {
			api.getCheapestRegion().then(function(data) {
				reply(data);
			})
		}
	}
];

var _       = require('lodash');
var request = require('request');

// Special data source
module.exports = {
	/**
	 * Data sources.
	 *
	 * @type {Array}
	 */
	sources: [{
		name: 'ondemand',
		url: 'http://a0.awsstatic.com/pricing/1/ec2/linux-od.min.js',
	}, {
		name: 'spot',
		url: 'http://spot-price.s3.amazonaws.com/spot.js'
	}],

	/**
	 * Available regions.
	 *
	 * @type {Array}
	 */
	regions: [
		'us-east', 'us-east-1', 'us-west', 'us-west-1', 'us-west-2',
		'eu-ireland', 'eu-west-1', 'eu-central-1', 'ap-southeast-1',
		'ap-southeast-2', 'ap-northeast-1', 'sa-east-1', 'us-gov-west-1',
		'apac-sin', 'apac-tokyo', 'apac-syd'
	],

	/**
	 * Get our regions.
	 *
	 * @return {Array} Regions.
	 */
	getRegions: function() {
		return this.regions;
	},

	/**
	 * Send a request for a single endpoint.
	 *
	 * @param  {String} name Source name.
	 * @param  {String} url  Source URL.
	 * @return {Promise}     Response data.
	 */
	sendRequest: function(name, url) {
		return new Promise(function(resolve, reject) {
			request(url, function(err, res, body) {
				var handleJsonp = new Function('callback', body);

				if (err) {
					reject(err);
				}

				return handleJsonp(function(data) {
					resolve({
						type: name,
						data: data
					});
				});
			});
		})
	},

	/**
	 * Get all our data.
	 *
	 * @return {Promise}
	 */
	get: function() {
		var self      = this;
		var resolvers = [];

		_.each(self.sources, function(source) {
			resolvers.push(self.sendRequest(source.name, source.url));
		});

		return Promise.all(resolvers).then(function(data) {
			return data;
		});
	}
};

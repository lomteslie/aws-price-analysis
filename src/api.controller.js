var dataSrc = require('./data');
var _       = require('lodash');

var ApiController = function() {
	var self = this;

	/**
	 * Get prices by region.
	 *
	 * @param  {String} regionType Region to gather prices by.
	 * @return {Promise}           Price data.
	 */
	this.getPricesByRegion = function(regionType) {
		return new Promise(function(resolve, reject) {
			dataSrc.get().then(function(data) {
				var regions = findWhere(data, 'region', regionType);
				var prices  = [];

				_.each(regions, function(region) {
					instances = _.map(region.instanceTypes, function(instance) {
						return instance.sizes;
					});

					prices = prices.concat.apply(prices, instances);
				});

				resolve({
					region: regionType,
					data: prices
				});
			});
		});
	};

	/**
	 * Get prices by instance type.
	 *
	 * @param  {String} instanceType Type of instance to gather prices by.
	 * @return {Promise}             Price data.
	 */
	this.getPricesByInstance = function(instanceType) {
		return new Promise(function(resolve, reject) {
			dataSrc.get().then(function(data) {
				var instances = findWhere(data, 'type', instanceType);
				var response  = iterateAndCondense(instances, function(instance) {
					return instance.sizes
				});

				resolve({
					instance: instanceType,
					data: response
				});
			});
		});
	};

	/**
	 * Return the top 10 price per vCPU instances across all regions.
	 *
	 * @return {Promise} Price data.
	 */
	this.getTopInstancesByVCPU = function() {
		return new Promise(function(resolve, reject) {
			dataSrc.get().then(function(data) {
				var regionTypes = dataSrc.getRegions();
				var regions = instances = prices = response = [];

				// Filter down into our regions...
				regions = iterateAndCondense(regionTypes, function(region) {
					return findWhere(data, 'region', region);
				});

				// Then our instances...
				instances = iterateAndCondense(regions, function(region) {
					return region.instanceTypes;
				});

				// And our prices
				prices = iterateAndCondense(instances, function(instance) {
					if (instance) {
						return instance.sizes;
					}
				});

				// Get the prices of all instances that have a vCPU property
				var response = _.chain(prices)
					.filter(function(price) {
						return _.has(price, 'vCPU');
					}).map(function(price) {
						return price;
					}).value();

				// Sort by price per vCPU
				response.sort(function(a, b) {
					var priceA = getLinuxPrice(a);
					var priceB = getLinuxPrice(b);

					return pricePerVCPU(a, priceA) - pricePerVCPU(b, priceB);
				});

				response = response.slice(0, 10);

				resolve({
					data: response
				});
			});
		});
	};

	/**
	 * Return the data for the cheapest region.
	 *
	 * @return {Promise} Price data.
	 */
	this.getCheapestRegion = function() {
		return new Promise(function(resolve, reject) {
			dataSrc.get().then(function(data) {
				var regionTypes = dataSrc.getRegions();
				var regions     = [];
				var prices      = [];

				// Filter down into our regions...
				regions = iterateAndCondense(regionTypes, function(region) {
					return findWhere(data, 'region', region);
				});

				prices = _.chain(regions)
					// Make sure we're working under regions
					.filter(function(region) {
						return region.region !== undefined;
					})
					// Map our prices averages to our regions
					.map(function(region) {
						var prices     = find(region, 'USD');
						var average    = 0;
						var priceCount = 0;

						_.each(prices, function(price) {
							var amount = parseFloat(price.USD);

							if (! _.isNumber(amount) || _.isNaN(amount)) {
								return false;
							}

							average += amount;
							priceCount++;
						});

						average = average / priceCount;

						return {
							region: region.region,
							average: average
						};
					}).value();

					// Sort our prices by average
					prices.sort(function(a, b) {
						return a.average - b.average;
					});

					// Get our cheap-o region and serve it up!
					self.getPricesByRegion(prices[0].region).then(function(data) {
						resolve(data);
					});
			});
		});
	};

	/**
	 * Iterate through the properties of an object and find a specific key.
	 *
	 * @param  {Object} obj   Object to iterate through.
	 * @param  {String} key   Key to match on the object.
	 * @return {Object|Array}
	 */
	function find(obj, key) {
		if (_.has(obj, key)) {
			return obj;
		}

		return _.flatten(_.map(obj, function(v) {
			return typeof v == "object" ? find(v, key) : [];
		}), true);
	}

	/**
	 * Iterate through the properties of an object,
	 * and check to make sure a certain key => value pair exists.
	 *
	 * @param  {Object} obj   Object to iterate through.
	 * @param  {String} key   Key to match on the object.
	 * @param  {String} value Value to match against the key.
	 * @return {Object|Array}
	 */
	function findWhere(obj, key, value) {
		if (_.has(obj, key) && obj[key] == value) {
			return obj;
		}

		return _.flatten(_.map(obj, function(v) {
			return typeof v == "object" ? findWhere(v, key, value) : [];
		}), true);
	}

	/**
	 * Iterate through a given array and condense the results into a new array.
	 *
	 * @param  {Array}    iterables Array to iterate over.
	 * @param  {Function} callback  Logic to iterate with.
	 * @return {Array}
	 */
	function iterateAndCondense(iterables, callback) {
		var iterated = response = [];

		_.each(iterables, function(iterable) {
			iterated.push(callback(iterable));
		});

		return response.concat.apply(response, iterated);
	}

	/**
	 * Return the price for the linux instance.
	 *
	 * @param  {Object} parent
	 * @return {Number}
	 */
	function getLinuxPrice(parent) {
		return _.map(parent.valueColumns, function(column) {
			if (column.name === 'linux') {
				return column.prices.USD;
			}
		})[0];
	}

	/**
	 * Get the price per vCPU.
	 *
	 * @param  {Object} parent Server instance.
	 * @param  {Number} price  USD price.
	 * @return {Number}
	 */
	function pricePerVCPU(parent, price) {
		return price / parent.vCPU;
	}
};

module.exports = ApiController;
// Main app entry point
var Hapi   = require('hapi');
var server = new Hapi.Server();
var argv   = require('minimist')(process.argv.slice(2))
var port   = argv.port || 9001;
var routes = require('./routes');

// Set up our server and start it up
server.connection({ port: port });
server.route(routes);
server.start(function() {
	console.log('Server listening on port ' + port);
});
